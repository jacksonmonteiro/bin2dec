function converter() {
    const binary = document.getElementById('binary').value;
    //const cria um variável de valor fixo usada somente para leitura
    //não significa que seu valor é imutável
    if (binary === '') return alert('Por favor, digite um número binário')
    //str.split divide a string em um array, separando a em substrings
    //array.map invoca a função callback, que devolve um nono array como resultado
    binary.split('').map((char) => {
        if (char !== '0' && char !== '1') return alert('Por favor, digite um número binário')
    })
    const decimal = parseInt(binary, 2)
    document.getElementById('decimal').value = decimal
    return true
}